/* 
## Задание

Написать реализацию игры ["Сапер"](http://minesweeper.odd.su/).

#### Технические требования:
- Нарисовать на экране поле 8*8 (можно использовать таблицу или набор блоков).
- Сгенерировать на поле случайным образом 10 мин. Пользователь не видит где они находятся.
- Клик левой кнопкой по ячейке поля "открывает" ее содержимое пользователю. 
  - Если в данной ячейке находится мина, игрок проиграл. В таком случае показать все остальные мины на поле. Другие действия стают недоступны, можно только начать новую игру. 
  - Если мины нет, показать цифру - сколько мин находится рядом с этой ячейкой.
  - Если ячейка пустая (рядом с ней нет ни одной мины) - необходимо открыть все соседние ячейки с цифрами.
- Клик правой кнопки мыши устанавливает или снимает с "закрытой" ячейки флажок мины.
- После первого хода над полем должна появляться кнопка "Начать игру заново",  которая будет обнулять предыдущий результат прохождения и заново инициализировать поле.
- Над полем должно показываться количество расставленных флажков, и общее количество мин (например `7 / 10`).

#### Не обязательное задание продвинутой сложности:
- При двойном клике на ячейку с цифрой - если вокруг нее установлено такое же количество флажков, что указано на цифре этой ячейки, открывать все соседние ячейки.
- Добавить возможность пользователю самостоятельно указывать размер поля. Количество мин на поле можно считать по формуле `Количество мин = количество ячеек / 6`.

*/



document.querySelector('.wrapper').style.marginTop = `${Math.floor(window.innerHeight / 2) - 220}px`;

const container = document.querySelector('.container');
const btn = document.querySelector('.btn');
let bombsIndexArr = [];
let firstClick = false;

for(let i  = 0; i < 64; i++){
    addCell(i);
}

placeBombs();

let cellArr = document.querySelectorAll('.cell');

bombsIndexArr.forEach(function(i) {
    cellArr[i].classList.add('bomb');
    // cellArr[i].style.backgroundColor = 'red';
} );

let bombs = document.querySelectorAll('.bomb');

container.addEventListener('click', function(event) {
    let targ = event.target;
    forClick(targ);
    console.log(document.querySelectorAll('.zero-class').length);
    OpenAroundZero(event.target);

})

container.addEventListener('contextmenu', function(event) {
    event.preventDefault();
    if(!firstClick){
        btn.style.display = 'block';
        document.querySelector('.wrapper').style.marginTop = `${Math.floor(window.innerHeight / 2) - 282}px`
        firstClick = true;
    }
    if (event.target.innerHTML == ''){
        event.target.classList.toggle('flags-a'); 
        document.querySelector('.flags-count').innerHTML = document.querySelectorAll('.flags-a').length;
    }     
})

btn.addEventListener('click', () => location.reload());





function OpenAroundZero(el) {
        let arrCellsAround = getArrCellsAround(el);
        if(getNumberBombsClose(arrCellsAround) == 0){
            arrCellsAround.forEach((e) => {
                if(!cellArr[e].classList.contains('num')){
                   forClick(cellArr[e]);
                };
           })
        }
} 


function addCell(i) {
    let x = document.createElement('div');
    x.className = 'cell';
    x.id = `id-${i}`;
    x.style.height = '50px';
    x.style.width = '50px'
    x.style.backgroundColor = 'white';
    x.style.boxSizing = 'border-box';
    x.style.border = '1px solid black';
    x.style.float = 'left';
    x.style.cursor = 'pointer';
    container.append(x);
}


function placeBombs() {
    let amount = 0;
      while(amount < 10) {
        let bombsIndex = Math.floor(Math.random() * document.querySelectorAll('.cell').length);
        if (bombsIndexArr.indexOf(bombsIndex)  == -1){
            bombsIndexArr.push(bombsIndex);
            amount++;
        }
    }
}


function getNumberBombsClose(arrCellsAround) {
    let counter = 0;
    arrCellsAround.forEach((el) => {
        bombsIndexArr.forEach((el2) => {
            if (el == el2){
                counter++;
            }
        })
    })
    return counter;
}


function getArrCellsAround(targ) {
    let idArr = [];
    let regExp = /\d+/;
    let targId = parseInt(targ.id.match(regExp));
    switch(targId){
        case 0: {
            idArr = [1, 8, 9];
            break;
        }
        case 7: {
            idArr = [6, 14, 15];
            break;
        }
        case 56: {
            idArr = [48, 49, 57];
            break;
        }
        case 63: {
            idArr = [54, 55, 62];
            break;
        }
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6: {
            idArr.push(targId - 1);
            idArr.push(targId + 1);
            idArr.push(targId + 7);
            idArr.push(targId + 8);
            idArr.push(targId + 9);
            break;
        }
        case 57:
        case 58:
        case 59:
        case 60:
        case 61:
        case 62: {
                idArr.push(targId - 1);
                idArr.push(targId + 1);
                idArr.push(targId - 7);
                idArr.push(targId - 8);
                idArr.push(targId - 9);
                break;
            }
        case 8:
        case 16:
        case 24:
        case 32:
        case 40:
        case 48:{
            idArr.push(targId + 1);
            idArr.push(targId - 7);
            idArr.push(targId - 8);
            idArr.push(targId + 8);
            idArr.push(targId + 9);
            break;
        }
        case 15:
        case 23:
        case 31:
        case 39:
        case 47:
        case 55:{
            idArr.push(targId - 1);
            idArr.push(targId - 8);
            idArr.push(targId - 9);
            idArr.push(targId + 7);
            idArr.push(targId + 8);
            break;
        }
        default: {
            idArr.push(targId - 1);
            idArr.push(targId + 1);
            idArr.push(targId - 7);
            idArr.push(targId - 8);
            idArr.push(targId - 9);
            idArr.push(targId + 7);
            idArr.push(targId + 8);
            idArr.push(targId + 9);
        }
    } 
    return idArr;
}


function forClick(targ) {
    if(!firstClick){
        btn.style.display = 'block';
        document.querySelector('.wrapper').style.marginTop = `${Math.floor(window.innerHeight / 2) - 282}px`
        firstClick = true;
    }
    if(!targ.classList.contains('flags-a')){
        let x  = document.createElement('div');
        x.style.height = '50px';
        x.style.width = '50px'
        x.style.boxSizing = 'border-box';
        if(targ.classList.contains('bomb')){
            bombs.forEach((el) => {
                el.style.backgroundImage = "url('img/bomb.png')";
                el.style.backgroundSize = '100%';
            })
            document.querySelector('.game-over').style.display = 'block'
            document.querySelector('.transp-bcg').style.display = 'block'
        } else {
            x.style.backgroundSize = '100%';
            x.style.textAlign = 'center';
            x.style.lineHeight = '51px';
            x.style.overflow = 'hidden';
            x.innerHTML = getNumberBombsClose(getArrCellsAround(targ));
            if(x.innerHTML == '0'){
                x.classList.add('zero-class');
            } 
        }
        targ.append(x);
        if (x.innerHTML != '0'){
            x.parentNode.classList.add('num');
        } 
    }
}










